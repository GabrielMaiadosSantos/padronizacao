import flet as ft
from flet import Page, View, AppBar, Text, flet

Value_negativos_input = ft.TextField(label="Nome da coluna")

def close_dlg(e):
    dlg.open = False
    Page.update()

def close_dlg_Fechar(e):
    Value_negativos_input.value = ""
    Value_negativos.value = False
    dlg.open = False
    Page.update()

dlg = ft.AlertDialog(
        content=Value_negativos_input, on_dismiss=lambda e: print("Fechado"),
        actions=[ft.TextButton("Adicionar", on_click=close_dlg), ft.TextButton(
            "Fechar", on_click=close_dlg_Fechar)],
)

def open_dlg(e):
    if (Value_negativos.value == True):
        Value_negativos_input.value = ""
        Page.dialog = dlg
        dlg.open = True
        Page.update()

Value_negativos = ft.Checkbox(
    label="Valor negativo", value=False, on_change=open_dlg)

    # TODO: Fazer conversor de data
Value_Data_Column = ft.TextField(label="Digite a coluna do arquivo")
Value_Data = ft.TextField(label="Digite o forma da data")

def close_dlg_data(e):
    dlg_Data.open = False
    Page.update()

def close_dlg_Fechar_data(e):
    Value_Data.value = ""
    Value_Data_Column.value = ""
    Padrao_data.value = False
    dlg_Data.open = False
    Page.update()

dlg_Data = ft.AlertDialog(
    content=ft.Column([Value_Data_Column, Value_Data], height=140), on_dismiss=lambda e: print("Fechado"),
    actions=[ft.TextButton("Adicionar", on_click=close_dlg_data), ft.TextButton(
        "Fechar", on_click=close_dlg_Fechar_data)],
)

def open_dlg_data(e):
    if (Padrao_data.value == True):
        Value_Data.value = ""
        Value_Data_Column.value = ""
        Page.dialog = dlg_Data
        dlg_Data.open = True
        Page.update()

Value_negativos = ft.Checkbox(
    label="Valor negativo", value=False, on_change=open_dlg)

Padrao_moeda = ft.Checkbox(label="Padrão de moeda", value=False)
Padrao_porcentagem = ft.Checkbox(
    label="Padrão de porcentagem", value=False)
Padrao_data = ft.Checkbox(label="Padrão de data",
                            value=False, on_change=open_dlg_data)
Itens_Nan = ft.Checkbox(label="Itens Nan", value=False)
