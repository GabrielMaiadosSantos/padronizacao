import flet as ft
from flet import Page, View, AppBar, Text, flet
from biblioteca_mazars import unificar_planilhas
import pandas as pd
import os


def Juntar(page, Menu):

    files_value = ""

    def pick_files_result(e: ft.FilePickerResultEvent):
        selected_files.value = (
            "".join(map(lambda f: f.path, e.files)
                    ) if e.files else "Cancelled!"
        )
        selected_files.update()

    pick_files_dialog = ft.FilePicker(on_result=pick_files_result)
    selected_files = ft.Text()

    def pick_files_result_second(e: ft.FilePickerResultEvent):
        selected_files_second.value = (
            "".join(map(lambda f: f.path, e.files)
                    ) if e.files else "Cancelled!"
        )
        selected_files_second.update()

    pick_files_dialog_second = ft.FilePicker(
        on_result=pick_files_result_second)
    selected_files_second = ft.Text()

    def FuncaoPrincipal(e):
        nome_valor = InputNome.value
        pasta1 = selected_files.value
        pasta2 = selected_files_second.value
        print(nome_valor)
        print(pasta1)
        print(pasta2)

        lista_arquivos = os.listdir()

        nome = str(nome_valor) + '.xlsx'
        if (nome != ""):
            Pasta1Read = pd.read_excel(pasta1)
            Pasta2Read = pd.read_excel(pasta2)
            arr = [Pasta1Read, Pasta2Read]
            df = pd.DataFrame(unificar_planilhas(arr))
            df.to_excel(str(nome), index="false")
            # os.rename(f"./{nome}", f"./Salvos/{nome}")
            open_dlg(nome)
            Menu(e)

    def open_dlg(e):
        page.dialog = dlg
        dlg.open = True
        page.update()

    def close_dlg(e):
        print("teste")
        dlg.open = False
        page.update()

    dlg = ft.AlertDialog(
        title=ft.Text("Arquivo gerado com sucesso", color="Green", size=40), actions=[
            ft.TextButton("Fechar", on_click=close_dlg),
        ],
    )

    InputNome = ft.TextField(label="Nome do arquivo")
    page.overlay.append(pick_files_dialog)
    page.overlay.append(pick_files_dialog_second)
    return [
        ft.Row([
            ft.Row(
                [
                    ft.Container(content=ft.Column(
                        [
                            ft.Container(content=ft.Image(src=f"/assets/logo.png"),
                                 padding=20,
                                 width=170,
                                         ),

                            ft.Container(
                                margin=10,
                                height=70,

                            ),
                            ft.Container(content=InputNome,
                                         margin=10,
                                         width=500,
                                         height=50

                                         ),
                            ft.Container(content=ft.ElevatedButton(
                                "SELECIONE O PRIMEIRO ARQUIVO",
                                icon=ft.icons.UPLOAD_FILE,
                                on_click=lambda _: pick_files_dialog.pick_files(
                                    allow_multiple=True
                                )),
                                margin=10,
                                width=500,
                            ),
                            selected_files,
                            ft.Container(content=ft.ElevatedButton(
                                "SELECIONE O SEGUNDO ARQUIVO",
                                icon=ft.icons.UPLOAD_FILE,
                                on_click=lambda _: pick_files_dialog_second.pick_files(
                                    allow_multiple=True
                                )),
                                margin=10,
                                width=500,
                            ),
                            selected_files_second,

                            ft.Container(content=ft.FilledButton(text="Finalizar", on_click=FuncaoPrincipal),
                                         margin=10,
                                         width=500,
                                         ),

                        ],
                        alignment=ft.MainAxisAlignment.START,
                        width=500,
                        height=680
                    )
                    ),
                    ft.Container(content=ft.Column([
                        ft.Row([

                            ft.Container(
                                ft.IconButton(
                                    icon=ft.icons.MENU,
                                    icon_color="white",
                                    icon_size=20,
                                    tooltip="Menu",
                                    on_click=Menu,
                                    width=30,
                                    height=50,
                                ), margin=20
                            )
                        ], alignment=ft.MainAxisAlignment.END),

                        ft.Column([
                            ft.Column([

                                ft.Container(content=ft.Text(
                                    "Unificação de arquivos", color="WHITE", size=40,  weight=ft.FontWeight.BOLD,), margin=20
                                ),

                                ft.Container(content=ft.Text(
                                    "Para facilitar, em alguns projetos unificamos o arquivo para ter que rodar um algoritimo ou fazer uma analize apenas uma vez.", color="WHITE", size=20, width=600), margin=20
                                ),
                            ]),

                            ft.Container(content=ft.Image(
                                src=f"/icons/pdf_conversor.png",
                                width=300,
                                fit=ft.ImageFit.CONTAIN,
                            ),
                            )
                        ],  horizontal_alignment=ft.CrossAxisAlignment.CENTER, width=1000),
                    ], alignment=ft.MainAxisAlignment.START),
                        bgcolor="BLUE",
                        width=760,
                        height=700,
                        margin=0,
                    ),
                ])
        ])

    ]
