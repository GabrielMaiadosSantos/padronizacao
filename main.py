import flet
import flet as ft
from flet import AppBar, ElevatedButton, Page, Text, View, colors

#pages:
from home import Home
from menu import Menu
from projetos import Projetos
from juntar import Juntar


def main(page: Page):
    page.title = "Routes Example"
    print("Initial route:", page.route)
  

    def route_change(e):
        print("Route change:", e.route)
        page.views.clear()
        page.views.append(
            View(
                "/",
                Home(open_settings))
        )
        if page.route == "/menu":
            page.views.append(
                View(
                    "/menu",
                   Menu(ProjetosFunc, JuntarFunc)
                )
            )
        if page.route == "/menu/projetos":
            page.views.append(
                View(
                    "/menu/projetos",
                   Projetos(page,open_settings)
                )
            )
        if page.route == "/menu/juntar":
            page.views.append(
                View(
                    "/menu/juntar",
                   Juntar(page,open_settings)
                )
            )
        page.update()

    def view_pop(e):
        print("View pop:", e.view)
        page.views.pop()
        top_view = page.views[-1]
        page.go(top_view.route)

    page.on_route_change = route_change
    page.on_view_pop = view_pop

    def open_settings(e):
        page.go("/menu")
        
    def JuntarFunc(e):
        page.go("/menu/juntar")
        
    def ProjetosFunc(e):       
        page.go("/menu/projetos")

    page.go(page.route)


flet.app(target=main, assets_dir="assets")
