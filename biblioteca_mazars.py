#Versão 1.0 - 23/01/2023

import pandas as pd

def unificar_planilhas(array):
#Descrição: unificar planilhas com o mesmo cabeçalho em um único dataframe
#Input : array = array de dataframes a serem unificados
#Exemplo de input: 
#                   array = [df1,df2,df3,df4]
#                   df_unificado = biblioteca_mazars.unificar_planilhas(array)
    tamanho = len(array)
    marcador = 0
    df_concat = array[0]
    while(marcador<tamanho):
        if(marcador > 0):
            df_concat = pd.concat([df_concat,array[marcador]])
        marcador = marcador + 1
    return(df_concat)

def valores_negativos(df,coluna):
#Descrição: filtrar valores negativos em coluna numérica
#Input: df = dataframe utilizado, coluna = coluna que vai ser utilizada para o filtro
#Exemplo de input:
#                   df['Custo']
#                   df_valores_negativos = biblioteca_mazars.valores_negativos(df,'Custo')
#                   obs: nome da colua sempre como string(entre aspas)
    df_retorno = df[df[coluna]<0]
    return(df_retorno)

#Descrição: filtrar valores iguais a zero em coluna numérica
#Input: df = dataframe utilizado, coluna = coluna que vai ser utilizada para o filtro
#Exemplo de input:
#                   df['Custo']
#                   df_valores_zerados = biblioteca_mazars.valores_zero(df,'Custo')
#                   obs: nome da colua sempre como string(entre aspas)


def valores_zero(df,coluna):
#Descrição: filtrar valores iguais a zero em coluna numérica
#Input: df = dataframe utilizado, coluna = coluna que vai ser utilizada para o filtro
#Exemplo de input:
#                   df['Custo']
#                   df_valores_zerados = biblioteca_mazars.valores_zero(df,'Custo')
#                   obs: nome da colua sempre como string(entre aspas)
    df_retorno = df[df[coluna]==0]
    return(df_retorno)


def valores_positivos(df,coluna):
#Descrição: filtrar valores positivos em coluna numérica
#Input: df = dataframe utilizado, coluna = coluna que vai ser utilizada para o filtro
#Exemplo de input:
#                   df['Custo']
#                   df_valores_positivos = biblioteca_mazars.valores_positivos(df,'Custo')
#                   obs: nome da colua sempre como string(entre aspas)
    df_retorno = df[df[coluna]>0]
    return(df_retorno)


def formatar_moeda(coluna):
#Descrição: formatar coluna como moeda, transformando para float
#Input: coluna = a coluna a ser formatada
#Exemplo de input:
#                   df['Custo']
#                   df['Custo'] = biblioteca_mazars.formatar_moeda(df['Custo'])
    coluna = coluna.astype(str)
    coluna = coluna.str.replace(",","v")
    #print(coluna)
    coluna = coluna.str.replace(".",",")
    #print(coluna)
    coluna = coluna.str.replace("v",".")
    #print(coluna)
    coluna = coluna.astype('float')
    return(coluna)


def formatar_data(coluna,formato):
#Descrição: formatar coluna como data, indicando o formato da data
#Input: coluna = coluna a ser formatada, formato = formatação da data ordem dia, mes, ano e separadores
#Exemplo de input:
#           df['Data'] = biblioteca_mazars.formatar_data(df['Data'],'%d/%m/%Y')
    coluna = pd.to_datetime(coluna, format=formato)
    return(coluna)


def contagem_ocorrencias(df,coluna):
#Descrição: conta quantas vezes/linhas contém cada valor/palavra 
#Input: df = dataframe utilizado e coluna = coluna a ser contabilizada
#Exemplo de input:
#                   df = biblioteca_mazars.contagem_ocorrencias(df,'Cod Empresa')
    df = df[[coluna]]
    df_resp = df.groupby([coluna]).size().reset_index()
    df_resp = df_resp.rename(columns = {0:'Contagem'})
    df_resp = df_resp.sort_values(by='Contagem', ascending=False)
    return(df_resp)

def filtrar_dados(df,coluna,valor):
#Descrição: filtra as linhas em que contém um valor especificado 
#Input: df = dataframe a ser filtrado, coluna = coluna que contém a informação do filtro e valor = varivel a ser utilizada como filtro
#Exemplo de input:
#                   df = bm.filtrar_dados(df,'Cod Emprsa',10)
    df_resp = df[df[coluna]==valor]
    return(df_resp)
