import flet as ft
from flet import Page, View, AppBar, Text, flet
import pandas as pd
from biblioteca_mazars import valores_negativos, valores_positivos, valores_zero, formatar_data, contagem_ocorrencias


def Projetos(page, Menu):

    def pick_files_result(e: ft.FilePickerResultEvent):
        selected_files.value = (
            "".join(map(lambda f: f.path, e.files)
                    ) if e.files else "Cancelled!"
        )
        selected_files.update()

    pick_files_dialog = ft.FilePicker(on_result=pick_files_result)
    selected_files = ft.Text()

    Value_negativos_input = ft.TextField(label="Nome da coluna")

    def close_dlg(e):
        dlg.open = False
        page.update()

    def close_dlg_Fechar(e):
        Value_negativos_input.value = ""
        Value_negativos.value = False
        dlg.open = False
        page.update()

    dlg = ft.AlertDialog(
        content=Value_negativos_input, on_dismiss=lambda e: print("Fechado"),
        actions=[ft.TextButton("Adicionar", on_click=close_dlg), ft.TextButton(
            "Fechar", on_click=close_dlg_Fechar)],
    )

    def open_dlg(e):
        if (Value_negativos.value == True):
            Value_negativos_input.value = ""
            page.dialog = dlg
            dlg.open = True
            page.update()

    Value_Positivo_input = ft.TextField(label="Nome da coluna")

    def close_dlg_positivo(e):
        dlg_positivo.open = False
        page.update()

    def close_dlg_Fechar_positivo(e):
        Value_Positivo_input.value = ""
        Value_Positivo.value = False
        dlg_positivo.open = False
        page.update()

    dlg_positivo = ft.AlertDialog(
        content=Value_Positivo_input, on_dismiss=lambda e: print("Fechado"),
        actions=[ft.TextButton("Adicionar", on_click=close_dlg_positivo), ft.TextButton(
            "Fechar", on_click=close_dlg_Fechar_positivo)],
    )

    def open_dlg_positivo(e):
        if (Value_Positivo.value == True):
            Value_Positivo_input.value = ""
            page.dialog = dlg_positivo
            dlg_positivo.open = True
            page.update()

    Value_Positivo = ft.Checkbox(
        label="Valor Positivo", value=False, on_change=open_dlg_positivo)

    # inicio
    Value_Moeda_input = ft.TextField(label="Nome da coluna")

    def close_dlg_Moeda(e):
        dlg_Moeda.open = False
        page.update()

    def close_dlg_Fechar_Moeda(e):
        Value_Moeda_input.value = ""
        Value_Moeda.value = False
        dlg_Moeda.open = False
        page.update()

    dlg_Moeda = ft.AlertDialog(
        content=Value_Moeda_input, on_dismiss=lambda e: print("Fechado"),
        actions=[ft.TextButton("Adicionar", on_click=close_dlg_Moeda), ft.TextButton(
            "Fechar", on_click=close_dlg_Fechar_Moeda)],
    )

    def open_dlg_Moeda(e):
        if (Value_Moeda.value == True):
            Value_Moeda_input.value = ""
            page.dialog = dlg_Moeda
            dlg_Moeda.open = True
            page.update()

    Value_Moeda = ft.Checkbox(label="Padrão de Moeda",
                              value=False, on_change=open_dlg_Moeda, disabled=False)

    # inicio
    Value_Zero_input = ft.TextField(label="Nome da coluna")

    def close_dlg_Zero(e):
        dlg_Zero.open = False
        page.update()

    def close_dlg_Fechar_Zero(e):
        Value_Zero_input.value = ""
        Value_Zero.value = False
        dlg_Zero.open = False
        page.update()

    dlg_Zero = ft.AlertDialog(
        content=Value_Zero_input, on_dismiss=lambda e: print("Fechado"),
        actions=[ft.TextButton("Adicionar", on_click=close_dlg_Zero), ft.TextButton(
            "Fechar", on_click=close_dlg_Fechar_Zero)],
    )

    def open_dlg_Zero(e):
        if (Value_Zero.value == True):
            Value_Zero_input.value = ""
            page.dialog = dlg_Zero
            dlg_Zero.open = True
            page.update()

    Value_Zero = ft.Checkbox(
        label="Valor Zero", value=False, on_change=open_dlg_Zero)

   # fim
    # inicio
    Value_Contagem_input = ft.TextField(label="Nome da coluna")

    def close_dlg_Contagem(e):
        dlg_Contagem.open = False
        page.update()

    def close_dlg_Fechar_Contagem(e):
        Value_Contagem_input.value = ""
        Value_Contagem.value = False
        dlg_Contagem.open = False
        page.update()

    dlg_Contagem = ft.AlertDialog(
        content=Value_Contagem_input, on_dismiss=lambda e: print("Fechado"),
        actions=[ft.TextButton("Adicionar", on_click=close_dlg_Contagem), ft.TextButton(
            "Fechar", on_click=close_dlg_Fechar_Contagem)],
    )

    def open_dlg_Contagem(e):
        if (Value_Contagem.value == True):
            Value_Contagem_input.value = ""
            page.dialog = dlg_Contagem
            dlg_Contagem.open = True
            page.update()

    Value_Contagem = ft.Checkbox(
        label="Ocorrencias", value=False, on_change=open_dlg_Contagem)

   # fim
    Value_Data_Column = ft.TextField(label="Digite a coluna do arquivo")
    Value_Data = ft.TextField(label="Digite o forma da data")

    def close_dlg_data(e):
        dlg_Data.open = False
        page.update()

    def close_dlg_Fechar_data(e):
        Value_Data.value = ""
        Value_Data_Column.value = ""
        Padrao_data.value = False
        dlg_Data.open = False
        page.update()

    dlg_Data = ft.AlertDialog(
        content=ft.Column([Value_Data_Column, Value_Data], height=140), on_dismiss=lambda e: print("Fechado"),
        actions=[ft.TextButton("Adicionar", on_click=close_dlg_data), ft.TextButton(
            "Fechar", on_click=close_dlg_Fechar_data)],
    )

    def open_dlg_data(e):
        if (Padrao_data.value == True):
            Value_Data.value = ""
            Value_Data_Column.value = ""
            page.dialog = dlg_Data
            dlg_Data.open = True
            page.update()

    Value_negativos = ft.Checkbox(
        label="Valor negativo", value=False, on_change=open_dlg)
    Padrao_data = ft.Checkbox(label="Padrão de data",value=False, on_change=open_dlg_data, disabled=False)

    def FuncaoPrincipal(e):
        nome_valor = InputNome.value
        negative_valor = Value_negativos.value
        padrao_moeda_valor = Value_Moeda.value
        padrao_data_valor = Padrao_data.value
        files_value = selected_files.value
        contagem_value = Value_Contagem.value

        PastaRead = pd.read_excel(files_value)
        df = pd.DataFrame(PastaRead)

        print(nome_valor)
        print(negative_valor)
        print(padrao_data_valor)
        print(padrao_moeda_valor)
        print(files_value)
        print(contagem_value)

        arr = []

        # aqui fazemos a tratativa de dados nescessária.
        if (Value_negativos.value == True):
            arr.append(valores_negativos(df, str(Value_negativos_input.value)))
        elif (Value_Positivo.value == True):
            arr.append(valores_positivos(df, str(Value_negativos_input.value)))
        elif (Value_Zero.value == True):
            arr.append(valores_zero(df, str(Value_negativos_input.value)))
        elif (Value_Contagem.value == True):
            arr.append(contagem_ocorrencias(df, str(Value_Contagem_input.value)))
        print(arr)
        nome = str(nome_valor) + '.xlsx'
        df = pd.DataFrame(arr[0])
        df.to_excel(nome, index="false")

    InputNome = ft.TextField(label="Nome do arquivo")
    page.overlay.append(pick_files_dialog)
    return [
        ft.Row([
            ft.Row(
                [
                    ft.Container(content=ft.Column(
                        [
                            ft.Container(content=ft.Image(src=f"/assets/logo.png"),
                                 padding=20,
                                 width=170,
                                         ),

                            ft.Container(
                                margin=10,
                                height=70,

                            ),
                            ft.Container(content=InputNome,
                                         margin=10,
                                         width=500,
                                         height=50

                                         ),
                            ft.Container(content=ft.ElevatedButton(
                                "SELECIONE UM ARQUIVO",
                                icon=ft.icons.UPLOAD_FILE,
                                on_click=lambda _: pick_files_dialog.pick_files(
                                    allow_multiple=True
                                )),
                                margin=10,
                                width=500,
                            ),
                            selected_files,


                            ft.Row([
                                ft.Column([
                                    ft.Container(content=Value_negativos,
                                                 margin=10,
                                                 ),
                                    ft.Container(content=Value_Positivo,
                                                 margin=10,
                                                 ),
                                ]),
                                ft.Column([
                                    ft.Container(content=Value_Moeda,
                                                 margin=10,
                                                 ),
                                    ft.Container(content=Padrao_data,
                                                 margin=10,
                                                 ),
                                ]),
                                ft.Column([
                                    ft.Container(content=Value_Zero,
                                                 margin=10,
                                                 ),
                                    ft.Container(content=Value_Contagem,
                                                 margin=10,
                                                 ),
                                ])

                            ], alignment=ft.MainAxisAlignment.SPACE_BETWEEN, width=500,),


                            ft.Container(content=ft.FilledButton(text="Finalizar", on_click=FuncaoPrincipal),
                                         margin=10,
                                         width=500,
                                         ),

                        ],
                        alignment=ft.MainAxisAlignment.START,
                        width=500,
                        height=680
                    )
                    ),
                    ft.Container(content=ft.Column([
                        ft.Row([

                            ft.Container(
                                ft.IconButton(
                                    icon=ft.icons.MENU,
                                    icon_color="white",
                                    icon_size=20,
                                    tooltip="Menu",
                                    on_click=Menu,
                                    width=30,
                                    height=50,
                                ), margin=20
                            )
                        ], alignment=ft.MainAxisAlignment.END),

                        ft.Column([
                            ft.Column([

                                ft.Container(content=ft.Text(
                                    "Padronização", color="WHITE", size=40,  weight=ft.FontWeight.BOLD,), margin=20
                                ),

                                ft.Container(content=ft.Text(
                                    "Ao iniciar projetos, seguimos um padrão para facilitar na hora de realizar as análises e formatações de dados", color="WHITE", size=20, width=600), margin=20
                                ),
                            ]),

                            ft.Container(content=ft.Image(
                                src=f"/icons/projetos.png",
                                width=300,
                                fit=ft.ImageFit.CONTAIN,
                            ),
                            )
                        ],  horizontal_alignment=ft.CrossAxisAlignment.CENTER, width=1000),
                    ], alignment=ft.MainAxisAlignment.START),
                        bgcolor="BLUE",
                        width=760,
                        height=700,
                        margin=0,
                    ),
                ])
        ])

    ]
