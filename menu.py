import flet as ft


def Menu(ProjetosFunc, Juntar):
    return [
        ft.Container(content=ft.Image(src=f"/assets/logo.png"),
                     margin=20,
                     width=150,
                     ),
        ft.Row([
            ft.Container(
                content=ft.Text(value="Menu", size=50,
                                color="#0A1F8F",  weight=ft.FontWeight.BOLD,),
            ),
        ], alignment=ft.MainAxisAlignment.CENTER),

        ft.Row(
            [


                ft.Container(content=ft.Container(
                    content=ft.Column([
                        ft.Container(content=ft.Text(
                            "Juntar Aquivos", size=40, color="WHITE"), margin=10,
                        ),
                        ft.Container(content=ft.Image(
                            src=f"/icons/pdf_conversor.png",
                            width=250,
                            height=250,
                            fit=ft.ImageFit.CONTAIN,
                        ),
                            margin=10,
                            width=500,
                        ),

                    ], alignment=ft.MainAxisAlignment.START, expand=True),
                    width=400,
                    height=400,
                    bgcolor="#00C0F9",
                    border_radius=10,
                    on_click=Juntar,
                ),
                ),



                ft.Container(content=ft.Container(
                    content=ft.Column([
                        ft.Container(content=ft.Text(
                            "Projetos", size=40, color="WHITE"), margin=10,),
                        ft.Container(content=ft.Image(
                            src=f"/icons/projetos.png",
                            width=250,
                            height=250,
                            fit=ft.ImageFit.CONTAIN,
                        ),
                            margin=10,
                            width=500,
                        ),

                    ], alignment=ft.MainAxisAlignment.START, expand=True),
                    width=400,
                    height=400,
                    bgcolor="#00C0F9",
                    border_radius=10,
                    on_click=ProjetosFunc,
                ),
                ),
            ],
            alignment=ft.MainAxisAlignment.CENTER,
            spacing=200,
            expand=True,
        )]
