import flet as ft
from flet import Page, View, AppBar, Text, flet
from menu import Menu


def Home(open_settings):
       return [
        ft.Container(content=ft.Image(src=f"/assets/logo.png"),
                     margin=20,
                     width=150,
                     ),
        ft.Row(
            [
                ft.Column([

                    ft.Container(content=ft.Image(src=f"/assets/teste.gif"),
                                 margin=20,
                                 width=500,
                                 ),

                ], alignment=ft.MainAxisAlignment.START, expand=True),
                ft.Column([
                    ft.Container(height=100
                                 ),
                    ft.Container(content=ft.Text("Mazars digital", size=68, color="#0A1F8F"),
                                 width=500,
                                 ),
                    ft.Container(content=ft.Text(
                        "Seja bem vindo à plataforma de transformação digital Mazars", size=20, color="#0071CE"
                    ),
                        width=600,
                    ),
                    ft.Container(
                        width=500,
                        height=30,
                    ),

                    ft.FilledButton(
                        "Entrar", on_click=open_settings, width=200, height=50),

                ], alignment=ft.MainAxisAlignment.START, expand=True),
            ],
            expand=True,
        ),
        ]
